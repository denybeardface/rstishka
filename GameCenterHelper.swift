//
//  GameCenterHelper.swift
//  rstishka
//
//  Created by Denis Davydov on 04/06/2019.
//  Copyright © 2019 Kody. All rights reserved.
//

import GameKit

struct Achievement {
    static let ActivateShoot = GKAchievement(identifier: "activateShoot")
    static let ActivateRace = GKAchievement(identifier: "activateRace")
    static let ActivateRoom = GKAchievement(identifier: "activateRoom")
    static let ActivateSelfie = GKAchievement(identifier: "activateSelfie")
    static let NULL = GKAchievement(identifier: "null")
}

final class GameCenterHelper: NSObject {
    typealias CompletionBlock = (Error?) -> Void
    static let helper = GameCenterHelper()
    var viewController: UIViewController?
    static var isAuthenticated: Bool {
        return GKLocalPlayer.local.isAuthenticated
    }
    
    static var playerInfo: GKLocalPlayer {
        return GKLocalPlayer.local
    }
    
    func loadAchievements() {
        
        GKAchievement.loadAchievements() { achievements, error in
            guard let achievements = achievements else { return }
            print("GAME loading achievements, count \(achievements.count)")
            NotificationCenter.default.post(name: .achievementsLoaded, object: achievements)
        }
    }
    
    func sendAchievement(_ achievement: GKAchievement){
        achievement.percentComplete = Double(100)
        achievement.showsCompletionBanner = true
        print(achievement)
        GKAchievement.report([achievement], withCompletionHandler: {
            error in
            if error != nil {
                print("Error: \(error)")
                return
            }
            print("GAME should load achievemnts")
            
//            self.loadAchievsxements()
            
        })
    }
    
    func resetAchievements(){
        GKAchievement.resetAchievements(completionHandler: { error in
            if error != nil {
                print(error?.localizedDescription)
            } else {
                print("achievements reseted")
                self.loadAchievements()
            }
        })
    }
    
    override init() {
        super.init()
        
        print("Loading gamecenter")
        GKLocalPlayer.local.authenticateHandler = { gcAuthVC, error in
            NotificationCenter.default
                .post(name: .authenticationChanged, object: GKLocalPlayer.local.isAuthenticated)
            
            if error != nil {
                print("Error authentication to GameCenter: " +
                    "\(error?.localizedDescription ?? "none")")
            }
            if GKLocalPlayer.local.isAuthenticated {
                print("Authenticated to Game Center!")
                self.loadAchievements()
            } else {
                if gcAuthVC != nil {
                    print("should show gc dialog")
                    self.viewController?.present(gcAuthVC!, animated: true)
                } else {
                    print("GCVC is \(gcAuthVC)")
                }
                
            }
            
        }
    }
}

extension Notification.Name {
    static let presentGame = Notification.Name(rawValue: "presentGame")
    static let authenticationChanged = Notification.Name(rawValue: "authenticationChanged")
    static let achievementsLoaded = Notification.Name(rawValue: "achievementsLoaded")
}
