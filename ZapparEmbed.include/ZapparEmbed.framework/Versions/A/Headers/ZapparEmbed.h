//
//  ZapparEmbed.h
//  ZapparEmbed
//
//  Copyright (c) 2012 Zappar Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ZapparDelegate<NSObject>

-(void)zapparClose;

@optional
-(void)zapparExperienceDidSendMessage:(NSString*)msg;
-(void)zapparUIDidLoad;
-(void)zapparAnalyticsExperienceSessionDidStart:(NSString*)deepLinkId;
-(void)zapparAnalyticsExperienceSessionDidEnd:(NSString*)deepLinkId;
-(void)zapparAnalyticsExperienceSession:(NSString*)deepLinkId didSendCustomEvent:(NSString*) eventName;
-(void)zapparEmbedFinished: (NSString*)withToken __deprecated;
@end

@protocol ZapparEmbedFinished<ZapparDelegate>
@end

@protocol ZapparViewController
@property (nonatomic, retain) void (^onMessage)(NSString* message);
@property (nonatomic, copy) void (^onClosed)();
@property (nonatomic, copy) void (^onPackageLoad)(NSString * id);
@property (nonatomic, copy) void (^onNoPackage)();
-(void)reset;
-(void) launchFromDeepLink:(NSString*)link;
@end

@interface ZapparEmbedOptions : NSObject {
}

@property (nonatomic, copy) UIColor * barColor;
@property (nonatomic) BOOL showPhotoButton;
@property (nonatomic) BOOL showGifButton;
@property (nonatomic) BOOL showSwitchCameraButton;
@property (nonatomic) BOOL showRescanButton;
@property (nonatomic) BOOL forceLandscape;
@property (nonatomic) BOOL showStatusBar;
@property (nonatomic) BOOL showFullScreen;
@property (nonatomic) BOOL showCameraAtStart;
@property (nonatomic) BOOL showHistoryButton;
@property (nonatomic) BOOL showFavoriteButton;
@property (nonatomic, copy) NSString * hostAppData;

+(ZapparEmbedOptions*)defaultOptions;
@end

@interface ZapparEmbed : NSObject {

}

+ (bool)isDeviceCompatible;
+ (UIViewController<ZapparViewController>*)viewControllerWithDelegate:(id<ZapparDelegate>)d;
+ (UIViewController<ZapparViewController>*)viewControllerWithDelegate:(id<ZapparDelegate>)d embedOptions:(ZapparEmbedOptions*)opts;

+ (UIViewController*)viewControllerWithFinish:(id<ZapparEmbedFinished>)f contentKey:(NSString*)key __deprecated;
+ (UIViewController*)preloadViewControllerWithFinish:(id<ZapparEmbedFinished>)f preloadKey:(NSString*)key __deprecated;
+ (UIViewController<ZapparViewController>*)zapcodeViewControllerWithFinish:(id<ZapparEmbedFinished>)f __deprecated;
+ (UIViewController<ZapparViewController>*)zapcodeViewControllerWithFinish:(id<ZapparEmbedFinished>)f embedOptions:(ZapparEmbedOptions*)opt __deprecated;
+(void)clearAnalyticsID;
+(void)initEmbed;

@end


