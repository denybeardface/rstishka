=== Zappar Embed Component ===


Below are instructions for embedding the Zappar component into your app.
The ZapparEmbedTest project is included with this file to demonstrate how this is done.
Most of the ZapparEmbed related code is in the ViewController.m file.



--- Embed Instructions ---



1 - Copy ZapparEmbed.include into your project directory.

Don't worry that the directory is over 3 MB – the compiled app will be much smaller!
Ensure that the minimum SDK version is at least 4.3.



2 – Add ZapparEmbed.include to your project

To do this in Xcode:
  1. Right click on your project name in the Project navigator on the left of the screen
  2. Select “Add Files to...”
  3. Select ZapparEmbed.include
  4. Ensure that “Create groups for any added folders” is selected
  5. Press “Add”



3 - Modify Build Settings

Under Project → Build Settings, search for Other Linker Flags and add the following line:
-ObjC



4 - Add API key to Info

Add an entry to the Info.plist file for your project.
Key: ZapparEmbedKey
Type: String
Value: <your embed key>

The embed key is tied to your bundle identifier so get in touch if you need to embed in an app with a different id.



5 - Add the following frameworks to your project

AddressBook
AddressBookUI
AudioToolbox
AVFoundation
CFNetwork
CoreMedia
CoreMotion
CoreVideo
EventKit
EventKitUI
libxml2.ylib
libz.dylib
MediaPlayer
MessageUI
MobileCoreServices
OpenAL
OpenGLES
QuartzCore
SystemConfiguration
Twitter    -- IMPORTANT - MARK TWITTER AS "OPTIONAL" FOR iOS 4.* COMPATIBILITY




6 - Modify your ViewController to start the Zappar component

Import the following headers into your ViewController header file:
#import <ZapparEmbed/ZapparEmbed.h>
#import "TargetConditionals.h"

Modify your ViewController to adopt the ZapparEmbedFinished protocol, e.g:
@interface ViewController : UIViewController<ZapparEmbedFinished>

Initialize and display the component in your ViewController, e.g:
  #if !(TARGET_IPHONE_SIMULATOR)
  UIViewController * zappar = [ZapparEmbed zapcodeViewControllerWithFinish:self];
  [self presentModalViewController:zappar animated:YES];
  #endif

This UIViewController is marked for autorelease and should work correctly in ARC projects.

Implement the -(void)zapparEmbedFinished: method to close the embed component e.g.:
  -(void)zapparEmbedFinished: (NSString*)withToken
  {
    [self dismissModalViewControllerAnimated:YES];
  }
  

6 - Add Transport Security Exception

Add the following entry to your Info.plist file:

App Transport Security Settings
 > Allow Arbitrary Loads: YES


Please do get in touch if there are any issues:
support@zappar.com

Good luck!

The Zappar Team