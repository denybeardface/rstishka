notDecemberEvent = scene:getById("notDecemberEvent")

setupevent = event.new("fireDateEvent")
runfire = runscript.new('text/x-lua')
runfire:setCode("FireDateEvent()")
setupevent:appendNode(runfire)
scene:appendNode(setupevent)


function FireDateEvent()

	timetable = os.date('*t')

	if timetable['month'] ~= 12 then
		notDecemberEvent:trigger()
		return
	end

	eventName = "day" .. timetable['day']
	local eventNode = scene:getById(eventName)
	eventNode:trigger()
end