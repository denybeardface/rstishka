            grp3x2 = scene:getById("puzzle_3x2")
            
            
            pieces_3x2 = {}
            piece_pos_3x2 = {}
            gap_row_3x2 = 0
            gap_col_3x2 = 0
            
            x_pos_3x2 = {-1,  0, -1, 0, -1, 0 }
            y_pos_3x2 = {-1, -1,  0, 0,  1, 1 }
            winning_positions_3x2 = {1, 5, 4, 6, 3, 1}
            
            --x_pos = {-0.75, -0.25, 0.25, 0.75, -0.75, -0.25, 0.25, 0.75, -0.75, -0.25, 0.25, 0.75, -0.75, -0.25, 0.25, 0.75}
            --y_pos = {-0.75, -0.75, -0.75, -0.75, -0.25, -0.25, -0.25, -0.25, 0.25, 0.25, 0.25, 0.25, 0.75, 0.75, 0.75, 0.75}
            function reset_3x2()

                winning_event_3x2 = scene:getById("onwin_3x2")

                gap_row_3x2 = 0
                gap_col_3x2 = 0

                x_pos_3x2 = {-1,  0, -1, 0, -1, 0 }
                y_pos_3x2 = {-1, -1,  0, 0,  1, 1 }

                for i = 1, 12 do
                    piece_pos_3x2[i] = i
                end

                grp3x2:replaceChildren(group.new())
                
    			for i = 2, 6 do
                    pieces_3x2[i] = object.new("primitives://plane.aro")
                    pieces_3x2[i]:setScale({0.4545, 0.4545, 0.4545})
                    pieces_3x2[i]:setPosition({x_pos_3x2[i], y_pos_3x2[i], 0.005})
                    pieces_3x2[i]:setSkin("3x2/" .. i .. ".jpg")
                
                    local hotspotCall = runscript.new("text/x-lua")
                    hotspotCall:setCode("obj_clicked_3x2(" .. i .. ")")
                    local onHitEvent = event.new()
                    onHitEvent:appendNode(hotspotCall)
                    pieces_3x2[i]:setEvent("onclickdown", onHitEvent)
                
                    grp3x2:appendNode(pieces_3x2[i])
    			end
            end
			
			function obj_clicked_3x2(id)
            	local pos_id = piece_pos_3x2[id];
            	local row = math.floor((pos_id - 1) / 2);
            	local col = (pos_id - 1) % 2;
            	if gap_row_3x2 == row then
            		if math.abs(gap_col_3x2 - col) == 1 then
            			gap_id = gap_row_3x2 * 2 + gap_col_3x2 + 1
            			doTransition_3x2(pieces_3x2[id], {x_pos_3x2[gap_id], y_pos_3x2[gap_id], 0.01})
            			piece_pos_3x2[id] = gap_id
            			gap_row_3x2 = row
            			gap_col_3x2 = col
            		end
            	else
            		if gap_col_3x2 == col then
            			if math.abs(gap_row_3x2 - row) == 1 then
            				gap_id = gap_row_3x2 * 2 + gap_col_3x2 + 1
            				doTransition_3x2(pieces_3x2[id], {x_pos_3x2[gap_id], y_pos_3x2[gap_id], 0.01})
            				piece_pos_3x2[id] = gap_id
            				gap_row_3x2 = row
            				gap_col_3x2 = col
            			end
            		end
            	end
            	checkForWin_3x2()
			end
            
            function doTransition_3x2(obj, destination)
            local evt = event.new()
            local tr = transition.new(obj, "position")
            tr:setEndValues(destination)
            tr:setLength(300)
            tr:setType(transition.SMOOTH)
            evt:appendNode(tr)
            scene:appendNode(evt)
            evt:trigger()
            end
            
            function checkForWin_3x2()
                printPositions_3x2()
                for i = 1, 6 do
                    if piece_pos_3x2[i] ~= winning_positions_3x2[i] then
                        return
                    end
                end
                -- We must have won
                winning_event_3x2:trigger()
            end
            
            function printPositions_3x2()
                local str = "P: "
                for i = 1, 6 do
                    str = str .. piece_pos_3x2[i] .. " ";
                end
                --print(str)
            end
