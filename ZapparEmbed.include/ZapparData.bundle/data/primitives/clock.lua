--clock works like this:
--Lua creates a runclock event which will replace all
--the children in the "runclockholder" event in the scene.
--The runclock event contains essentially independent blocks
--for second, minute and hour hands and the triggering of 15-minute
--events "on0mins", "on15mins", etc. All of those blocks contain an
--initial delay or transition (based on the time between the current
--time and the base position) and then an infinitely looped transition
--for each normal hand rotation (or hour sequence of event triggers)

function SetupClock(time_scale, initial_time)
  timetable = os.date('*t')
  runclock = event.new('runclock')
  con = concurrent.new()
  
  --set up second hand block
  sec_hand = scene:getById('secondhand')
  sec_seq = createHandSequential(sec_hand, timetable['sec'], 60, time_scale, initial_time)
  con:appendNode(sec_seq)
  
  --minute hand block
  secs_through_hour = timetable['min'] * 60 + timetable['sec']
  min_hand = scene:getById('minutehand')
  min_seq = createHandSequential(min_hand, secs_through_hour, 3600, time_scale, initial_time)
  con:appendNode(min_seq)
  
  --hour hand block
  secs_through_halfday = (timetable['hour']%12) * 3600 + secs_through_hour
  hour_hand = scene:getById('hourhand')
  hour_seq = createHandSequential(hour_hand, secs_through_halfday, 43200, time_scale, initial_time)
  con:appendNode(hour_seq)
  
  --events
  secs_through_fifteen_mins = (timetable['min'] % 15) * 60 + timetable['sec']
  triggers_seq = sequential.new()
  first_wait = wait.new()
  first_wait:setLength((900 - secs_through_fifteen_mins) * time_scale + initial_time)
  triggers_seq:appendNode(first_wait)
  triggers_main_seq = sequential.new()
  next_event = math.floor(timetable['min'] / 15) + 1
  for ev_offset = 0, 3 do
    event_mins = ((next_event + ev_offset) % 4) * 15
    triggers_main_seq:appendNode(trigger.new(scene:getById('on' .. event_mins .. 'mins')))
    normal_wait = wait.new()
    normal_wait:setLength(900 * time_scale)
    triggers_main_seq:appendNode(normal_wait)
  end
  triggers_main_seq:setNoTimes(-1)
  triggers_seq:appendNode(triggers_main_seq)
  con:appendNode(triggers_seq)
  
  runclock:appendNode(con)
  scene:getById('runclockholder'):replaceChildren(runclock)
  runclock:trigger()
end

function createHandSequential(hand, fraction, total, time_scale, initial_time)
  seq = sequential.new()
  init_pos = fraction * (360.0 / total)
  init_trans = transition.new(hand, 'rotation')
  init_trans:setLength(initial_time)
  init_trans:setEndValues({init_pos, 0, 0})
  init_trans:setType(transition.SMOOTH)
  seq:appendNode(init_trans)
  first_trans = transition.new(hand, 'rotation')
  first_trans:setLength((total - fraction) * time_scale)
  first_trans:setInitValues({init_pos, 0, 0})
  first_trans:setEndValues({360, 0, 0})
  seq:appendNode(first_trans)
  normal_trans = transition.new(hand, 'rotation')
  normal_trans:setLength(total * time_scale)
  normal_trans:setInitValues({0, 0, 0})
  normal_trans:setEndValues({360, 0, 0})
  normal_trans:setNoTimes(-1)
  seq:appendNode(normal_trans)
  return seq
end

--This runs on load and inserts a "setupclock" event into
--the scene.
setupevent = event.new('setupclock')
runsetup = runscript.new('text/x-lua')
runsetup:setCode("SetupClock(1000, 1000)")
setupevent:appendNode(runsetup)
scene:appendNode(setupevent)

setupevent = event.new('setupclockx50')
runsetup = runscript.new('text/x-lua')
runsetup:setCode("SetupClock(20, 1000)")
setupevent:appendNode(runsetup)
scene:appendNode(setupevent)

setupevent = event.new('setupclockx200')
runsetup = runscript.new('text/x-lua')
runsetup:setCode("SetupClock(5, 1000)")
setupevent:appendNode(runsetup)
scene:appendNode(setupevent)

setupevent = event.new('setupclockinstant')
runsetup = runscript.new('text/x-lua')
runsetup:setCode("SetupClock(1000, 0)")
setupevent:appendNode(runsetup)
scene:appendNode(setupevent)

setupevent = event.new('setupclockx50instant')
runsetup = runscript.new('text/x-lua')
runsetup:setCode("SetupClock(20, 0)")
setupevent:appendNode(runsetup)
scene:appendNode(setupevent)

setupevent = event.new('setupclockx200instant')
runsetup = runscript.new('text/x-lua')
runsetup:setCode("SetupClock(5, 0)")
setupevent:appendNode(runsetup)
scene:appendNode(setupevent)