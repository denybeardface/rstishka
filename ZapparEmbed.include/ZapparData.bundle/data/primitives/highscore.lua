--str = {"MYSTRING", "2"}
--package:storeTable(str, "mystring")
--newstr = package:restoreTable("mystring")
--assert(false, newstr)


function displayHighScores(idbase, key, namespace)
	local scores = package:restoreTable(key, namespace)
    local rows = {}
	local i = 1
	while true do
		node = scene:getById(idbase .. ":" .. i .. ":name")
		if node == nil then
			break
		end
		rows[i] = {txt = scene:getById(idbase .. ":" .. i .. ":name"), score = scene:getById(idbase .. ":" .. i .. ":score")}
		thisrow = scores[i]
		if thisrow ~= nil then
			rows[i].txt:setText(thisrow.name)
			rows[i].score:setText(thisrow.score)
		else
			rows[i].txt:setText("")
			rows[i].score:setText("")
		end
		i = i + 1
	end
	--assert(false, "Nodes: " .. i-1)
end

function addScore(nametextid, scoretextid, inverted, key, namespace)
    local scores = package:restoreTable(key, namespace)
	local nametext = scene:getById(nametextid)
	local scoretext = scene:getById(scoretextid)
    table.insert(scores, {name=nametext:getText(), score=scoretext:getText(), when=os.time()})
	table.sort(scores, function (a, b)
        local ascore = tonumber(a.score)
        local bscore = tonumber(b.score)
        if ascore == bscore then
            return a.when > b.when
        end
        if inverted == true then
            return ascore < bscore
        end
        return ascore > bscore
    end)
	package:storeTable(scores, key, namespace)
end

function addScoreTriggerHigh(nametextid, scoretextid, inverted, iftop, eventToTrigger, key, namespace)
	local nametext = scene:getById(nametextid):getText()
	local scoretext = scene:getById(scoretextid):getText()
    
    addScore(nametextid, scoretextid, inverted, key, namespace)
    local scores = package:restoreTable(key, namespace)
    
    local searchnumber = iftop
    if searchnumber > table.getn(scores) then
        searchnumber = table.getn(scores)
    end

    for i=1,searchnumber do
        
        --assert(false, "TRYING " .. scores[i].name .. nametext)
        if scores[i].name == nametext then
            --assert(false, "PASSED NAME CHECK")
            if scores[i].score == scoretext then
                --assert(false, "PASSED SCORE CHECK")
                evt = scene:getById(eventToTrigger)
                if evt ~= nil then
                    evt:trigger()
                    return
                end
            end
        end
    end
end

function clearScores(key, namespace)
	local scores = {}
	package:storeTable(scores, key, namespace)
end