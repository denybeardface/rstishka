            group = scene:getById("group")
            winning_event = scene:getById("onwin")
            
            pieces = {}
            piece_pos = {}
            gap_row = 0
            gap_col = 0
            
            x_pos = {-1,  0,  1, -1, 0, 1, -1, 0, 1, -1, 0, 1}
            y_pos = {-1, -1, -1,  0, 0, 0,  1, 1, 1, 2, 2, 2}
            winning_positions = {1, 3, 12, 1, 8, 6, 4, 9, 11, 2, 10, 7}
            
            --x_pos = {-0.75, -0.25, 0.25, 0.75, -0.75, -0.25, 0.25, 0.75, -0.75, -0.25, 0.25, 0.75, -0.75, -0.25, 0.25, 0.75}
            --y_pos = {-0.75, -0.75, -0.75, -0.75, -0.25, -0.25, -0.25, -0.25, 0.25, 0.25, 0.25, 0.25, 0.75, 0.75, 0.75, 0.75}
            for i = 1, 12 do
            piece_pos[i] = i
            end
            
			for i = 2, 12 do
            pieces[i] = object.new("primitives://plane.aro")
            pieces[i]:setScale({0.4545, 0.4545, 0.4545})
            pieces[i]:setPosition({x_pos[i], y_pos[i], 0.005})
            pieces[i]:setSkin("chunk/" .. i .. ".jpg")
            
            local hotspotCall = runscript.new("text/x-lua")
            hotspotCall:setCode("obj_clicked(" .. i .. ")")
            local onHitEvent = event.new()
            onHitEvent:appendNode(hotspotCall)
            pieces[i]:setEvent("onclickdown", onHitEvent)
            
            group:appendNode(pieces[i])
			end
			
			function obj_clicked(id)
            pos_id = piece_pos[id];
            row = math.floor((pos_id - 1) / 3);
            col = (pos_id - 1) % 3;
            if gap_row == row then
            if math.abs(gap_col - col) == 1 then
            gap_id = gap_row * 3 + gap_col + 1
            doTransition(pieces[id], {x_pos[gap_id], y_pos[gap_id], 0.01})
            piece_pos[id] = gap_id
            gap_row = row
            gap_col = col
            end
            else
            if gap_col == col then
            if math.abs(gap_row - row) == 1 then
            gap_id = gap_row * 3 + gap_col + 1
            doTransition(pieces[id], {x_pos[gap_id], y_pos[gap_id], 0.01})
            piece_pos[id] = gap_id
            gap_row = row
            gap_col = col
            end
            end
            end
            checkForWin()
			end
            
            function doTransition(obj, destination)
            local evt = event.new()
            local tr = transition.new(obj, "position")
            tr:setEndValues(destination)
            tr:setLength(300)
            tr:setType(transition.SMOOTH)
            evt:appendNode(tr)
            scene:appendNode(evt)
            evt:trigger()
            end
            
            function checkForWin()
                printPositions()
                for i = 1, 12 do
                    if piece_pos[i] ~= winning_positions[i] then
                        return
                    end
                end
                -- We must have won
                winning_event:trigger()
            end
            
            function printPositions()
                local str = "P: "
                for i = 1, 12 do
                    str = str .. piece_pos[i] .. " ";
                end
                --print(str)
            end
