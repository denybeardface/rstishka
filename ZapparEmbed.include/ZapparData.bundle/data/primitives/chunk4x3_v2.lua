            grp4x3 = scene:getById("puzzle_4x3")
            winning_event_4x3 = scene:getById("onwin_4x3")
            
            pieces_4x3 = {}
            piece_pos_4x3 = {}
            gap_row_4x3 = 0
            gap_col_4x3 = 0
            
            x_pos_4x3 = {-1,  0,  1, -1, 0, 1, -1, 0, 1, -1, 0, 1}
            y_pos_4x3 = {-1, -1, -1,  0, 0, 0,  1, 1, 1, 2, 2, 2}
            winning_positions_4x3 = {1, 9, 11, 6, 1, 3, 4, 5, 7, 12, 8, 2}
            
            function reset_4x3()
                gap_row_4x3 = 0
                gap_col_4x3 = 0
            
                x_pos_4x3 = {-1,  0,  1, -1, 0, 1, -1, 0, 1, -1, 0, 1}
                y_pos_4x3 = {-1, -1, -1,  0, 0, 0,  1, 1, 1, 2, 2, 2}

                for i = 1, 12 do
                    piece_pos_4x3[i] = i
                end

                grp4x3:replaceChildren(group.new())
            
                for i = 2, 12 do
                    pieces_4x3[i] = object.new("primitives://plane.aro")
                    pieces_4x3[i]:setScale({0.4545, 0.4545, 0.4545})
                    pieces_4x3[i]:setPosition({x_pos_4x3[i], y_pos_4x3[i], 0.005})
                    pieces_4x3[i]:setSkin("4x3/" .. i .. ".jpg")
            
                    local hotspotCall = runscript.new("text/x-lua")
                    hotspotCall:setCode("obj_clicked_4x3(" .. i .. ")")
                    local onHitEvent = event.new()
                    onHitEvent:appendNode(hotspotCall)
                    pieces_4x3[i]:setEvent("onclickdown", onHitEvent)
            
                    grp4x3:appendNode(pieces_4x3[i])
                end
            end
			
			function obj_clicked_4x3(id)
                pos_id = piece_pos_4x3[id];
                row = math.floor((pos_id - 1) / 3);
                col = (pos_id - 1) % 3;
                if gap_row_4x3 == row then
                    if math.abs(gap_col_4x3 - col) == 1 then
                        gap_id = gap_row_4x3 * 3 + gap_col_4x3 + 1
                        doTransition4x3(pieces_4x3[id], {x_pos_4x3[gap_id], y_pos_4x3[gap_id], 0.01})
                        piece_pos_4x3[id] = gap_id
                        gap_row_4x3 = row
                        gap_col_4x3 = col
                    end
                else
                    if gap_col_4x3 == col then
                        if math.abs(gap_row_4x3 - row) == 1 then
                            gap_id = gap_row_4x3 * 3 + gap_col_4x3 + 1
                            doTransition4x3(pieces_4x3[id], {x_pos_4x3[gap_id], y_pos_4x3[gap_id], 0.01})
                            piece_pos_4x3[id] = gap_id
                            gap_row_4x3 = row
                            gap_col_4x3 = col
                        end
                    end
                end
                checkForWin4x3()
			end
            
            function doTransition4x3(obj, destination)
                local evt = event.new()
                local tr = transition.new(obj, "position")
                tr:setEndValues(destination)
                tr:setLength(300)
                tr:setType(transition.SMOOTH)
                evt:appendNode(tr)
                scene:appendNode(evt)
                evt:trigger()
            end
            
            function checkForWin4x3()
                printPositions4x3()
                for i = 1, 12 do
                    if piece_pos_4x3[i] ~= winning_positions_4x3[i] then
                        return
                    end
                end
                -- We must have won
                winning_event_4x3:trigger()
            end
            
            function printPositions4x3()
                local str = "P: "
                for i = 1, 12 do
                    str = str .. piece_pos_4x3[i] .. " ";
                end
                --print(str)
            end
