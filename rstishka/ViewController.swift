//
//  ViewController.swift
//  zappar
//
//  Created by Denis Davydov on 18/03/2019.
//  Copyright © 2019 Kody. All rights reserved.
//

/*
 
 (!) Библиотека ZapparFramework не содержит нужных символов для работы в эмуляторе.
 Запускать на реальном девайсе.
 
 dummy.mm - костыльный файл, не удалять
 
 */



import UIKit
import SwiftyJSON
import CoreData
import AVFoundation
import GameKit

class ViewController: UIViewController, UINavigationControllerDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    @IBOutlet weak var snapBtn: UIImageView!;
    var imageTake: UIImage!
    var imagePicker: UIImagePickerController!
    var recognized = JSON()
    var games = [Game]()
    var gcGames = [String]()
    
    let loader: UIView = {
        let view = UIView()
        let av = UIActivityIndicatorView()
        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25)
        av.translatesAutoresizingMaskIntoConstraints = false
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(av)
        
        av.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        av.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        av.startAnimating()
        return view
    }()
    
    @IBOutlet weak var gamePark: UIImageView!;
    @IBOutlet weak var gameShoot: UIImageView!;
    @IBOutlet weak var gameRace: UIImageView!;
    @IBOutlet weak var gameFear: UIImageView!;
    @IBOutlet weak var gameSelfie: UIImageView!;
    @IBOutlet weak var gameShootTtp: UIImageView!;
    @IBOutlet weak var gameRaceTtp: UIImageView!;
    @IBOutlet weak var gameFearTtp: UIImageView!;
    @IBOutlet weak var gameSelfieTtp: UIImageView!;
    @IBOutlet weak var errorMsg: UIView!;
    @IBOutlet weak var helpView: UIView!;
    @IBOutlet weak var logo: UIImageView!;
    @IBOutlet weak var logo2: UIImageView!;
    
    var pendingAnimations: [UIImageView] = []
    var player: AVAudioPlayer?
    var doNotClear = false
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "help", withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            guard let player = player else { return }
            
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    var helpBtn: UIButton = {
        let view = UIButton()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setImage(UIImage(named: "helpBtn"), for: .normal)
        
        return view
    }()
    
    enum ImageSource {
        case photoLibrary
        case camera
    }
    
    @objc func showHelpMessage() {
        self.helpView.fadeIn()
        self.helpView.isUserInteractionEnabled = true
        playSound()
        AnalyticsManager.shared.create(event: Event.Show_Hint)
    }
    
    @IBAction func closeHelpMessage() {
        self.helpView.fadeOutImmediate()
        self.helpView.isUserInteractionEnabled = false
        player?.stop()
    }

    func showError() {
        let alert = UIAlertController(title: nil, message: "Упс! Что-то пошло не так. Попробуй ещё раз!", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Закрыть", style: .default, handler: nil)
        
        alert.addAction(dismiss)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showRecognitionDuplicateError() {
        let alert = UIAlertController(title: nil, message: "Упс! Ранее ты уже делал фото с этим героем. Сфотографируй другого и получи титул Короля Парка!", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Закрыть", style: .default, handler: nil)
        
        alert.addAction(dismiss)
        
        present(alert, animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        AnalyticsManager.shared.uploadEvents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if gcGames.count == 0 {
            GameCenterHelper.helper.loadAchievements()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(takePhoto(_:)))
        snapBtn.addGestureRecognizer(tapGestureRecognizer)
        
        let selectParkRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        let selectShootRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        let selectRaceRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        let selectFearRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        let selectSelfieRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        
        gamePark.isUserInteractionEnabled = true
        gameShoot.isUserInteractionEnabled = true
        gameRace.isUserInteractionEnabled = true
        gameFear.isUserInteractionEnabled = true
        gameSelfie.isUserInteractionEnabled = true
        
        gamePark.addGestureRecognizer(selectParkRecognizer)
        gameShoot.addGestureRecognizer(selectShootRecognizer)
        gameRace.addGestureRecognizer(selectRaceRecognizer)
        gameFear.addGestureRecognizer(selectFearRecognizer)
        gameSelfie.addGestureRecognizer(selectSelfieRecognizer)
        
        self.view.addSubview(helpBtn)
        self.view.addSubview(loader)
        
        loader.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        loader.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0).isActive = true
        loader.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        loader.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        loader.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        loader.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        
        loader.alpha = 0
        
        helpBtn.widthAnchor.constraint(equalToConstant: 35).isActive = true
        helpBtn.heightAnchor.constraint(equalToConstant: 39).isActive = true
        helpBtn.imageView?.contentMode = .scaleAspectFit
        helpBtn.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -10).isActive = true
        helpBtn.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -10).isActive = true
        helpBtn.addTarget(self, action: #selector(showHelpMessage), for: .touchUpInside)
        
        helpView.alpha = 0
        let gRec = UITapGestureRecognizer(target: self, action: #selector(closeHelpMessage))
        helpView.addGestureRecognizer(gRec)
        
        let height = view.frame.size.height
        
        if let submitBtn = helpView.viewWithTag(555) as? UIButton {
            print("Height: \(height)")
            if height == 568 {
                submitBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50).isActive = true
            } else if height < 740 {
                submitBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -70).isActive = true
            } else {
                submitBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -150).isActive = true
            }
        }

        
        GameCenterHelper.helper.viewController = self
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(authenticationChanged(_:)),
            name: .authenticationChanged,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(achievementsLoaded(_:)),
            name: .achievementsLoaded,
            object: nil
        )
        NotificationCenter.default.addObserver(self, selector: #selector(externalAchievement(_:)), name: NSNotification.Name(rawValue: "external"), object: nil)
        
//        GameCenterHelper.helper.resetAchievements()
        
        if height == 568 {
            
            logo.scale(by:0.8)
            logo.translate(x: -20, y: -20)
            logo2.scale(by: 0.8)
            logo2.translate(x:0, y:40)
            
            snapBtn.scale(by: 0.8)
            snapBtn.translate(x:20, y:-20)
            
            gamePark.scale(by: 0.9)
            gamePark.translate(x: 20, y:0)
            
            gameShoot.scale(by: 0.9)
            gameShoot.translate(x: -20, y:0)
            gameShootTtp.scale(by: 0.9)
            gameShootTtp.translate(x:0, y: -20)
            
            gameRace.scale(by: 0.9)
            gameRace.translate(x: 20, y:-20)
            gameRaceTtp.scale(by: 0.9)
            //        gameRaceTtp.translate(x:0, y: 40)
            
            gameFear.scale(by: 0.9)
            gameFear.translate(x: -20, y:-20)
            gameFearTtp.scale(by: 0.9)
            //        gameFearTtp.translate(x:0, y: 40)
            
            gameSelfie.scale(by: 0.9)
            gameSelfie.translate(x: 0, y: -40)
            gameSelfieTtp.scale(by: 0.9)
            gameSelfieTtp.translate(x:0, y: -20)
        }
        
        activateGames()
    }
    
    @objc private func externalAchievement(_ notification: Notification) {
        let ach = notification.object as! GKAchievement
        doNotClear = true
        GameCenterHelper.helper.sendAchievement(ach)
        
        gcGames.removeAll()
        gcGames.append(Achievement.ActivateShoot.identifier)
        gcGames.append(Achievement.ActivateRace.identifier)
        gcGames.append(Achievement.ActivateRoom.identifier)
        activateGames()
    }
    
    @objc private func authenticationChanged(_ notification: Notification) {
//        onlineButton.isEnabled = notification.object as? Bool ?? false
//        print(GameCenterHelper.playerInfo)
    }
    
    @objc private func achievementsLoaded(_ notification: Notification) {
//        print(notification.object)
        if doNotClear {
            return
        }
        
        let typedArr = notification.object as! Array<GKAchievement>
        self.gcGames.removeAll()
        for ach in typedArr {
            self.gcGames.append(ach.identifier)
//            print("SELF GAMES \(self.gcGames)")
        }
        activateGames()
    }

    func activateGames() {
        DispatchQueue.main.async {
            
            switch self.gcGames.count {
            case 1:
                self.gameShoot.image = UIImage(named: "attr2")
            case 2:
                self.gameShoot.image = UIImage(named: "attr2")
                self.gameRace.image = UIImage(named: "attr3")
            case 3:
                self.gameShoot.image = UIImage(named: "attr2")
                self.gameRace.image = UIImage(named: "attr3")
                self.gameFear.image = UIImage(named: "attr4")
            case 4:
                self.gameShoot.image = UIImage(named: "attr2")
                self.gameRace.image = UIImage(named: "attr3")
                self.gameFear.image = UIImage(named: "attr4")
                self.gameSelfie.image = UIImage(named: "attr5")
            default:
                return
            }
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        let isUnlocked = tappedImage.tag <= gcGames.count
        
//        print("STATS: \(isUnlocked), tag: \(tappedImage.tag), \(gcGames.count)")

        let zapparEmbedOpt:ZapparEmbedOptions = ZapparEmbedOptions.default();
        let zappar = ZapparEmbed.viewController(with: self, embedOptions: zapparEmbedOpt)

        zappar?.onMessage = {(_ msg: String?) -> Void in
            if msg == "finish"{
                OperationQueue.main.addOperation {
                    zappar?.dismiss(animated: true, completion: nil)
                    if self.gcGames.count == 0 {
                        self.helpView.fadeIn()
                        self.helpView.isUserInteractionEnabled = true
                        self.playSound()
                    }
                }
            }

            if msg?.contains("launch") == true {
                let deeplink = msg?.split(separator: ":")[1]
                OperationQueue.main.addOperation {
                    zappar?.launch(fromDeepLink: "z/\(deeplink ?? "")")
                }
            }

            if msg?.contains("activate") == true {
                var achievement = Achievement.NULL

                switch msg! {
                case "activateShoot":
                    achievement = Achievement.ActivateShoot
                case "activateRace":
                    achievement = Achievement.ActivateRace
                case "activateRoom":
                    achievement = Achievement.ActivateRoom
                case "activateSelfie":
                    achievement = Achievement.ActivateSelfie
                default:
                    print("wrong activation msg")
                }
                GameCenterHelper.helper.sendAchievement(achievement)

                if !self.gcGames.contains(achievement.identifier){
                    self.gcGames.append(achievement.identifier)
                    self.activateGames()
                }

            }
        }

        switch tappedImage.tag {
        case 0:
            AnalyticsManager.shared.create(event: Event.Click_Amusement_Park)
            self.present(zappar!, animated: true, completion: nil)
            zappar?.launch(fromDeepLink: "z/iI3j1c")
        case 1:
            AnalyticsManager.shared.create(event: Event.Click_Shooting_Gallery)
            if !isUnlocked {
                AnalyticsManager.shared.create(event: Event.Show_Take_Picture_Wand)
                
                gameShootTtp.fadeIn()
                gameShootTtp.fadeOut()
                let last = pendingAnimations.popLast()
                last?.layer.removeAllAnimations()
                last?.fadeOutImmediate()
                
                if last == gameShootTtp {
                    gameShootTtp.fadeIn()
                    gameShootTtp.fadeOut()
                }
                pendingAnimations.append(gameShootTtp)
            } else {
                self.present(zappar!, animated: true, completion: nil)
                zappar?.launch(fromDeepLink: "z/SQyl1c")
            }
        case 2:
            AnalyticsManager.shared.create(event: Event.Click_Race)
            
            if !isUnlocked {
                AnalyticsManager.shared.create(event: Event.Show_June_21)
                gameRaceTtp.fadeIn()
                gameRaceTtp.fadeOut()
                let last = pendingAnimations.popLast()
                last?.layer.removeAllAnimations()
                last?.fadeOutImmediate()
                if last == gameRaceTtp {
                    gameRaceTtp.fadeIn()
                    gameRaceTtp.fadeOut()
                }
                pendingAnimations.append(gameRaceTtp)
            } else {
                let url = URL(string:"https://brainrus.ru/rstishkaRaceiOs")!
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        case 3:
            AnalyticsManager.shared.create(event: Event.Click_Room_Fear)
            
            if !isUnlocked {
                AnalyticsManager.shared.create(event: Event.Show_June_21)
                gameFearTtp.fadeIn()
                gameFearTtp.fadeOut()
                let last = pendingAnimations.popLast()
                last?.layer.removeAllAnimations()
                last?.fadeOutImmediate()
                if last == gameFearTtp {
                    gameFearTtp.fadeIn()
                    gameFearTtp.fadeOut()
                }
                pendingAnimations.append(gameFearTtp)
            } else {
                self.present(zappar!, animated: true, completion: nil)
                zappar?.launch(fromDeepLink: "z/2yOm1c")
            }
        case 4:
            AnalyticsManager.shared.create(event: Event.Click_Selfie)
            if !isUnlocked {
                AnalyticsManager.shared.create(event: Event.Show_June_21)
                gameSelfieTtp.fadeIn()
                gameSelfieTtp.fadeOut()
                let last = pendingAnimations.popLast()
                last?.layer.removeAllAnimations()
                last?.fadeOutImmediate()
                if last == gameSelfieTtp {
                    gameSelfieTtp.fadeIn()
                    gameSelfieTtp.fadeOut()
                }
                pendingAnimations.append(gameSelfieTtp)
            } else {
                self.present(zappar!, animated: true, completion: nil)
                zappar?.launch(fromDeepLink: "z/QVJk1c")
            }
        default:
            print("tapped spmething else")
        }
    }
    
    @objc func takePhoto(_ sender: UIButton) {
        AnalyticsManager.shared.create(event: Event.Click_Take_Picture)
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            selectImageFrom(.photoLibrary)
            return
        }
        selectImageFrom(.camera)
    }
    
    func selectImageFrom(_ source: ImageSource){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        switch source {
        case .camera:
            imagePicker.sourceType = .camera
            AnalyticsManager.shared.create(event: Event.Click_Photo)
        case .photoLibrary:
            AnalyticsManager.shared.create(event: Event.Use_Photo)
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }
}

extension ViewController: UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        imagePicker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }
        imageTake = selectedImage
        runRecognition()
    }
    
    func runRecognition() {
        loader.fadeIn();
        let resizedImage = imageTake.resizeUI(size: CGSize(width: 240, height: 480))
        let imageData:NSData = resizedImage!.pngData()! as NSData
        
        let url = URL(string:"https://api.clarifai.com/v2/searches")
        var request = URLRequest(url: url!)

        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Key 69b6426b5b5d4e0e805241af57e4bf57", forHTTPHeaderField: "Authorization")
        request.httpMethod = "Post"

        let imageb64 = ["base64":"\(imageData.base64EncodedString(options: .lineLength64Characters))"]
        let imageDict = ["image":imageb64]
        let data = ["data":imageDict]
        let input = ["input":data]
        let output = ["output": input]
        let ands = ["ands":[output]]

        let complete = ["query": ands]

        do {
            let jsonData = try JSONSerialization.data(withJSONObject: complete, options: .prettyPrinted)

            let _ = URLSession.shared.uploadTask(with: request, from: jsonData) {
                (data, response, error) in
                if error != nil {
                    OperationQueue.main.addOperation {
                        self.loader.fadeOutImmediate()
                        self.showError()
                    }
                    return
                }

                if let data = data {
                    guard let json = try? JSON(data: data) else {
                        print("malformed json")
                        return
                    }

                    let predictions = json["hits"].arrayValue
                    let relevant = predictions.filter {
                        (jsonObj) -> Bool  in
                            return jsonObj["input"]["data"]["concepts"][0]["name"].string == "package"
                    }

                    let match = relevant.sorted(by: {$0["score"] > $1["score"]})
                    let first = match[0]["score"].float ?? 0
                    if match.count == 0 || first <= 0.8 {
                        AnalyticsManager.shared.create(event: Event.Unsuccessful_Photos)
                        AnalyticsManager.shared.uploadEvents()
                        OperationQueue.main.addOperation {
                            self.loader.fadeOutImmediate()
                            self.showError()
                        }
                    } else {
                        AnalyticsManager.shared.create(event: Event.Successfull_Photos)
                        AnalyticsManager.shared.uploadEvents()
                        let request = Code.fetchRequest() as NSFetchRequest<Code>
                        let name = match[0]["input"]["data"]["metadata"]["name"].string
                        
                        request.predicate = NSPredicate(format: "name CONTAINS %@", name!)
                        
                        do {
                            let codeExists = try self.context.fetch(request)
                            
                            if codeExists.count == 0 {
                                if self.gcGames.count == 4 {
                                    OperationQueue.main.addOperation {
                                        self.loader.fadeOutImmediate()
                                    }
                                    return
                                }
                                
                                let code = Code(entity: Code.entity(), insertInto: self.context)
                                code.name = name!
                                self.appDelegate.saveContext()
                                var achievement = Achievement.NULL
                                
                                switch self.gcGames.count {
                                case 0:
                                    achievement = Achievement.ActivateShoot
                                case 1:
                                    achievement = Achievement.ActivateRace
                                case 2:
                                    achievement = Achievement.ActivateRoom
                                case 3:
                                    achievement = Achievement.ActivateSelfie
                                default:
                                    print("nothing happens")
                                }
                                
                                GameCenterHelper.helper.sendAchievement(achievement)
                                if !self.gcGames.contains(achievement.identifier) {
                                    self.gcGames.append(achievement.identifier)
                                    self.activateGames()
                                }
                                
                                OperationQueue.main.addOperation {
                                    self.loader.fadeOutImmediate()
                                }
                                
                            } else {
                                OperationQueue.main.addOperation {
                                    self.loader.fadeOutImmediate()
                                    self.showRecognitionDuplicateError()
                                }
                                return
                            }
                        } catch let error as NSError {
                            print("Error: \(error)")
                            OperationQueue.main.addOperation {
                                self.loader.fadeOutImmediate()
                                self.showError()
                            }
                        }
                    }

                }
            }.resume()

        } catch {
            print(error.localizedDescription)
        }
        
        
    }
    
}

extension UIView {
    
    func fadeIn(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 0.5, delay: TimeInterval = 4.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
    
    func fadeOutImmediate(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
}

extension ViewController: ZapparDelegate {
    func zapparClose() {
        self.dismiss(animated: true)
        AnalyticsManager.shared.create(event: Event.Click_Back)
    }
}

extension UIImage {
    func resizeUI(size:CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, true, self.scale)
        self.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: size))
        
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resizedImage
    }
}

extension UIView {
    func scale(by: CGFloat) {
        let origTransform = self.transform
        let newTransform = origTransform.scaledBy(x: by, y: by)
        
        self.transform = newTransform
    }
    
    func translate(x: CGFloat, y: CGFloat) {
        let origTransform = self.transform
        let newTransform = origTransform.translatedBy(x: x, y: y)
        
        self.transform = newTransform
    }
}
