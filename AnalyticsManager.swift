//
//  AnalyticsManager.swift
//  rstishka
//
//  Created by Denis Davydov on 24/05/2019.
//  Copyright © 2019 Kody. All rights reserved.
//

import Foundation
import AWSPinpoint

struct Event {
    static let Click_Amusement_Park = "Клик на иконку «Парк аттракционов»"
    static let Click_Shooting_Gallery = "Клик на иконку «Водный тир»"
    static let Click_Race = "Клик на иконку «Гонки»"
    static let Click_Room_Fear = "Клик на иконку «Комната страха»"
    static let Click_Selfie = "Клик на иконку «Селфи короля парка»"
    static let Click_Take_Picture = "Клик на кнопку «Сфотографируй палочку»"
    static let Click_Back = "Вернулись на главный экран из игры"
    static let Show_June_21 = "Показалась всплывашка «Доступно с 21 июня»"
    static let Show_Take_Picture_Wand = "Показалась всплывашка «Сфотографируй палочку»"
    static let Use_Photo = "Клик на кнопку Использовать фото"
    static let Click_Photo = "Клик на кнопку Фото"
    static let Successfull_Photos = "Число успешно обработанных фото"
    static let Unsuccessful_Photos = "Число неудачных обработанных фото"
    static let Show_Hint = "Показали подсказку"
}

class AnalyticsManager {
    
    static let shared = AnalyticsManager()
    private var pinpoint: AWSPinpoint?
    
    private init() { }
    
    func setupClient(withLaunchOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        pinpoint = AWSPinpoint(configuration:
            AWSPinpointConfiguration.defaultPinpointConfiguration(launchOptions: launchOptions))
    }
    
    func create(event: String) {
        guard let pinpoint = pinpoint else { return }
        
        let event = pinpoint.analyticsClient.createEvent(withEventType: event)
       
        pinpoint.analyticsClient.record(event)
    }
    
    func uploadEvents() {
        guard let pinpoint = pinpoint else { return }
        pinpoint.analyticsClient.submitEvents()
    }
    
}
