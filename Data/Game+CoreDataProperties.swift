//
//  Game+CoreDataProperties.swift
//  rstishka
//
//  Created by Denis Davydov on 28/05/2019.
//  Copyright © 2019 Kody. All rights reserved.
//
//

import Foundation
import CoreData


extension Game {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Game> {
        return NSFetchRequest<Game>(entityName: "Game")
    }

    @NSManaged public var image: String?
    @NSManaged public var unlocked: Bool
    @NSManaged public var url: String?

}
