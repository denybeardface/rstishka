//
//  Code+CoreDataProperties.swift
//  rstishka
//
//  Created by Denis Davydov on 28/05/2019.
//  Copyright © 2019 Kody. All rights reserved.
//
//

import Foundation
import CoreData


extension Code {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Code> {
        return NSFetchRequest<Code>(entityName: "Code")
    }

    @NSManaged public var name: String?

}
